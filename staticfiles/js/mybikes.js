var Store = {};


Store.bikes = [
	{
		id:54456,
		category:"Fixie/SS",
		img_url: "staticfiles/images/Bikes/Fixie:SS/ss1.jpg",
		brand:"Moots",
		description:"grey amazing",
		serial: "asdkjh88vn"
	},
	{
		id:54456,
		category:"Fixie/SS",
		img_url: "staticfiles/images/Bikes/Fixie:SS/ss3.jpg",
		brand:"Speedy",
		description:"Black bike",
		serial: "w987yfa"
	}
];

Store.start = function(){
	$(document).ready(function() {
		Store.loadProducts();
	});
};

Store.loadProducts = function(){
    console.log("load products");
	$("#content").empty();
	for(let i = 0; i < Store.bikes.length; i++){
        console.log("in loop");
		var productObj = Store.bikes[i];
		var product = $("#templates .product").clone();
		product.find(".img").css("background-image","url('"+ productObj.img_url +"')");
		product.find(".brand").text(productObj.brand);
        product.find(".desc").text(productObj.description);
        product.find(".serial").text(productObj.serial);		
		$("#content").append(product);
	}
	
};
Store.stolenEvent = function(){
	$('#stolenForm').css({'display':'initial'});
}


Store.start();