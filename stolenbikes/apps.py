from django.apps import AppConfig


class StolenbikesConfig(AppConfig):
    name = 'stolenbikes'
