from django.shortcuts import render

from django.http import HttpResponse
from django.template import loader
from stolenbikes import database
from stolenbikes import manageMap
from stolenbikes import models

def addBikeControl(request):
    print(request.GET)
    template = loader.get_template('stolenbikes\\views\\addBike.html')
    b = models.Bike( owner='FakeUser', serial_number=request.GET['serial'],
                     stolen_status=request.GET['stolen'], date_added='1996-02-01', bike_color='red', brand='None')
    b.save()
    context = {
        'subpage' : 'sucessfullyAdded'
    }
    return HttpResponse(template.render(context))

def index(request):
    template = loader.get_template('stolenbikes\\views\\index.html')
    context = {
        'subpage' : 'index'
    }
    return HttpResponse(template.render(context))

def bikeDisplay(request):
    template = loader.get_template('stolenbikes\\views\\bikes.html')
    bike_list = models.Bike.objects.all()
    print(bike_list )
    context = {
        'subpage' : 'bikes',
        'bikes' : bike_list,
    }
    return HttpResponse(template.render(context))
def register(request):
    template = loader.get_template('stolenbikes\\views\\register.html')
    context = {
    }
    return HttpResponse(template.render(context))

def addTheftForm(request):
    template = loader.get_template('stolenbikes\\views\\addTheft.html')
    context = {
    }
    return HttpResponse(template.render(context))

def addTheftControl(request):

    if 'bike_id' in request.GET:
        b = models.Theft(stolen_longitude='10', stolen_latitude='28.2', owner='FakeUser', bikeid=request.GET['bike_id'], recovered_status='LOST', date_reported_stolen='1/29/2018', tagline='All the latest Beatles news.')
    b.save()
def chart(request):
    template = loader.get_template('stolenbikes\\views\\indexbody.html')

    context = {
    }
    return HttpResponse(template.render(context))

def mapDisplay(request):
    template = loader.get_template('stolenbikes\\views\\map.html')
    vals = database.getData(database.connection())
    print(vals)
    means = manageMap.kmeans(vals)
    context = {
        'means': means,
    }
    return HttpResponse(template.render(context))

def map(request):
    template = loader.get_template('stolenbikes\\views\\map.html')
    context = {
    }
    return HttpResponse(template.render(context))