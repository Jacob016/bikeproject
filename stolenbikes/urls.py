from django.urls import path
import stolenbikes.views as views


urlpatterns = [
    path('', views.index, name='index'),
    path('index', views.index, name='index'),
    path('mapDisplay', views.mapDisplay, name='mapDisplay'),
    path('register', views.register, name='register'),
    path('map', views.map, name='map'),
    path('bikesShow', views.bikeDisplay, name='bikes'),
    path('addBike', views.addBikeControl, name='addbikes'),
    path('chart', views.chart, name='chart'),
]