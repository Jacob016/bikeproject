''' Python code managing the machine learning processing in order to display the risky locations
according to the database of stolen bikes
'''
import numpy as np
# If you'd like to use another package for interacting with MySQL it's fine. Just update this template accordingly.
import pymysql
from math import radians, cos, sin, asin, sqrt
from nltk.cluster.kmeans import KMeansClusterer
import timeit
import collections


def haversine(ptx, pty):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    """
    lon1, lat1 = ptx
    lon2, lat2 = pty
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * asin(sqrt(a))
    r = 6371  # Radius of earth in kilometers. Use 3956 for miles
    return c * r


def distance(pt1, pt2):
    # start_time = timeit.default_timer()
    haversine(pt1, pt2)


def kmeans(list):
    # code you want to evaluate
    NUM_CLUSTERS = int(len(list) / 10)
    print (list)
    vectors = zip([x['end_longitude'] for x in list], [x['end_latitude'] for x in list])
    vectors = [np.array(x) for x in vectors]
    kclusterer = KMeansClusterer(NUM_CLUSTERS, distance=haversine, repeats=25)
    clusters = kclusterer.cluster(vectors, assign_clusters=True)
    means = [x.tolist() for x in kclusterer.means()]
    print('Clustered:' + str(vectors))
    print('As:' + str(clusters))
    print('Means:' + str(means))
    counter = collections.Counter(clusters)
    weight = [x[1] for x in sorted(counter.items(), key=lambda i: i[0])]
    output =  [x for x in zip([y[0] for y in means], [y[1] for y in means], weight)]
    print(output)
    return output

def main():
    """ Main function permitting to call train and test functions"""

    pt1, pt2 = [-8.639244, 41.166027], [-8.617734, 41.146832]
    vals = distance(pt1, pt2)
    print(vals)
    return kmeans()


if __name__ == "__main__":
    main()