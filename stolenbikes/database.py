import pymysql
import matplotlib.pyplot as plot
import json
''' Database for fake bikes stolen'''
def connection():
    username = 'root'
    connection = pymysql.connect(host='localhost',
                                 user='root',
                                 password='toor',
                                 db='taxi',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    cur = connection.cursor()
    return cur

def getData(cur):
    cur.execute("SELECT DISTINCT end_latitude, end_longitude from trips WHERE end_latitude IS NOT NULL AND end_latitude IS NOT NULL limit 200;");
    position = cur.fetchall()
    return list(position)

def main():
    """ Main function permitting to call train and test functions"""
    vals = getData(connection())

if __name__ == "__main__":
    main()