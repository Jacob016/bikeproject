var Store = {};

Store.bikes = [
	{
		id:1234,
		category:"Electric",
		img_url: "staticfiles/images/Bikes/Electric/electric1.jpg",
		brand:"Magnum",
		description:"Dope bike",
		serial: "kjh7fwei4"
	},
	{
		id:1234,
		category:"Electric",
		img_url: "staticfiles/images/Bikes/Electric/electric2.jpg",
		brand:"Pedego",
		description:"My great bike",
		serial: "23kljfwei4"
	},
	{
		id:1234,
		category:"Road-Bikes",
		img_url: "staticfiles/images/Bikes/Road/road6.png",
		brand:"Cinelli",
		description:"Dope bike",
		serial: "23kljfwei4"
	},
	{
		id:3444,
		category:"Road-Bikes",
		img_url: "staticfiles/images/Bikes/Road/road7.jpg",
		brand:"Crumpton",
		description:"fastest bike on the road!",
		serial: "23fd555i4",
		stolen: true,			
	},
	{
		id:5644,
		category:"Mountain-Bikes",
		img_url: "staticfiles/images/Bikes/Mountain/mount4.jpg",
		brand:"Kona",
		description:"gonna shred some mountains",
		serial: "23kl6079",
		stolen: true,
	},
	{
		id:1254,
		category:"Mountain-Bikes",
		img_url: "staticfiles/images/Bikes/Mountain/mount5.jpg",
		brand:"Kona",
		description:"Blue bike",
		serial: "sdklj90vkl"
	},
	{
		id:5456,
		category:"Fixie/SS",
		img_url: "staticfiles/images/Bikes/Fixie:SS/fix1.jpg",
		brand:"Kona",
		description:"Black bike",
		serial: "89adsfjh"
	},
	{
		id:5456,
		category:"Fixie/SS",
		img_url: "staticfiles/images/Bikes/Fixie:SS/fix2.jpg",
		brand:"Custom",
		description:"Black bike",
		serial: "3423rfw",
		stolen: true,	
	},
	{
		id:54456,
		category:"Fixie/SS",
		img_url: "staticfiles/images/Bikes/Fixie:SS/ss1.jpg",
		brand:"Moots",
		description:"grey amazing",
		serial: "asdkjh88vn"
	},
	{
		id:54456,
		category:"Fixie/SS",
		img_url: "staticfiles/images/Bikes/Fixie:SS/ss3.jpg",
		brand:"Speedy",
		description:"Black bike",
		serial: "w987yfa"
	},
	{
		id:54456,
		category:"Child",
		img_url: "staticfiles/images/Bikes/Child/child5.jpg",
		brand:"Unknown",
		description:"light blue",
		serial: "masddm59",
		stolen: true,
	},
	{
		id:54456,
		category:"Child",
		img_url: "staticfiles/images/Bikes/Child/child6.jpg",
		brand:"Unknown",
		description:"green",
		serial: "f00dlsa"
	},
	{
		id:54456,
		category:"Child",
		img_url: "staticfiles/images/Bikes/Child/child7.jpg",
		brand:"Unknown",
		description:"pink and blue",
		serial: "ajia99"
	},
	{
		id:54456,
		category:"BMX",
		img_url: "staticfiles/images/Bikes/BMX/bmx6.jpg",
		brand:"Unknown",
		description:"white",
		serial: "hhu879g9"
	},
	{
		id:54456,
		category:"BMX",
		img_url: "staticfiles/images/Bikes/BMX/bmx7.jpg",
		brand:"Unknown",
		description:"green and black",
		serial: "qert679",
		stolen: true,
	},
	{
		id:54456,
		category:"BMX",
		img_url: "staticfiles/images/Bikes/BMX/bmx8.jpg",
		brand:"Unknown",
		description:"red",
		serial: "tgy479"
	}

];


Store.start = function(){
	$(document).ready(function() {
		Store.rowCounter = 0;
		Store.addSearchRow();
		Store.loadCategories();
		$("#search").click(Store.searchDirectorty);
	});
};

Store.loadCategories = function(){
		var categories = ["Electric","Road-Bikes","Mountain-Bikes","Fixie/SS","BMX","Child"];
		console.log(categories);
		var categoriesHolder = $("#categories");
		categoriesHolder.empty();
		for (i in categories){
			var categoryBtn = $("<nav />").addClass("nav-btn clickable").text(categories[i]);
			categoryBtn.attr("id",categories[i]);
			categoryBtn.click(function(e){
				Store.loadProducts($(this).attr("id"));
			});
			categoriesHolder.append(categoryBtn)
		} 
		categories[0];
		Store.loadProducts(categories[0]);
};

Store.loadProducts = function(category){
	console.log(category);
	$("#content").empty();
	for(let i = 0; i < Store.bikes.length; i++){
		if (Store.bikes[i].category == category){
			var productObj = Store.bikes[i];
			var product = $("#templates .product").clone();
			product.find(".img").css("background-image","url('"+productObj.img_url+"')");
			product.find(".brand").text(productObj.brand);
			product.find(".desc").text(productObj.description);
			product.find(".serial").text(productObj.serial);
			product.find("form input[name='item_name']").val(productObj.title);
			product.find("form input[name='item_number']").val(productObj.id);
			if (productObj.stolen){
				product.addClass("favorite");
			}
			$("#content").append(product);
		}
	}
	
};

Store.addSearchRow = function() {
	Store.rowCounter++;
	let mainSearchDiv = $("#searchParameters");
	let newRow = $('<div/>').addClass("parameterRow");
	newRow.appendTo(mainSearchDiv);
	$('<h5/>').attr({
		for:'sel'+Store.rowCounter,
		width:'75px'
	})
	.html("Select attribute:").appendTo(newRow);
	$('<div/>').addClass("dropList").appendTo(newRow);
	$('<select/>').addClass("form-control")
	.attr('id','sel'+Store.rowCounter)
	.appendTo(newRow).append('<option>brand</option><option>Size</option><option>Features</option><option>Serial Number</option>');
	$('<input/>').attr({
		type:"text", 
		id:"parameter"+Store.rowCounter
	}).addClass("searchInput form-control").appendTo(newRow);
	$('<button/>').attr("type","button")
	.click(Store.addSearchRow)
	.addClass("plusBtn addSearchTerm searchTermBtn")
	.text("+").appendTo(newRow);
} 

Store.searchDirectorty = function() {
	console.log("in search");
	$("#content").empty();
	for (let i = 1; i < (Store.rowCounter + 1); i++){
		sel = ($('#sel'+i).val()).toLowerCase();
		console.log(sel);
		param = ($('#parameter'+i).val());	
		for(let i = 0; i < Store.bikes.length; i++){
			if (Store.bikes[i][sel] == param){
				console.log(Store.bikes[i][sel])
				var productObj = Store.bikes[i];
				var product = $("#templates .product").clone();
				product.find(".img").css("background-image","url('"+productObj.img_url+"')");
				product.find(".brand").text(productObj.brand);
				product.find(".desc").text(productObj.description);
				product.find(".serial").text(productObj.serial);
				product.find("form input[name='item_name']").val(productObj.title);
				product.find("form input[name='item_number']").val(productObj.id);
				if (productObj.stolen){
					product.addClass("favorite");
				}
				$("#content").append(product);
			}
		}
	}	
}

Store.start();