from django.db import models

class Bike(models.Model):
    serial_number = models.CharField(max_length=200)
    brand = models.CharField(max_length=200)
    date_added = models.DateTimeField('date added')
    stolen_status = models.CharField(max_length=200)
    bike_color = models.CharField(max_length=200)
    owner = models.CharField(max_length=200)

class Theft(models.Model):
    id_theft = models.CharField(max_length=200)
    stolen_longitude = models.CharField(max_length=200)
    stolen_latitude = models.CharField(max_length=200)
    owner = models.CharField(max_length=200)
    bikeid = models.ForeignKey(Bike, on_delete=models.CASCADE)
    recovered_status = models.CharField(max_length=200)
    date_reported_stolen = models.DateTimeField('date stolen')

class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)